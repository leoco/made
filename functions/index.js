const functions = require("firebase-functions");
const admin = require("firebase-admin");
const sgMail = require("@sendgrid/mail");
const cors = require("cors")({ origin: true });
admin.initializeApp();

exports.sendMail = functions.https.onRequest((req, res) => {
	cors(req, res, () => {

		const data = req.body.data;
		const from = data.from;
		const message = data.message;
		const subject = data.subject;

		sgMail.setApiKey(functions.config().sendgrid.key);
		const msg = {
			to: "rolland.madisson@gmail.com",
			from: from,
			subject: "Site: " + subject,
			text: message,
		};

		return sgMail.send(msg, false, (erro, info) => {
			if (erro) {
				return res.status(500).json(erro);
			}
			return res.status(200).json({data: {}});
		});
	});
});
