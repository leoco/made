import 'package:flutter/material.dart';

import 'package:made/routes.dart';

void main() {
  ProjectRoutes.initializeRoutes();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Mad(e)',
      theme: ThemeData(
        fontFamily: 'Muli',
        primaryColor: Colors.black,
        accentColor: Colors.white,
      ),
      debugShowCheckedModeBanner: false,
      initialRoute: '/',
      onGenerateRoute: ProjectRoutes.appRouter.generator,
    );
  }
}
