import 'package:flutter/material.dart';

import 'package:made/Model/Project.dart';
import 'package:made/View/ProjectList/Footer/web/FooterWeb.dart';
import 'package:responsive_builder/responsive_builder.dart';

class Footer extends StatelessWidget {
  Footer(
      {Key key, this.height, this.width, this.projects, this.projectController})
      : super(key: key);
  final double height;
  final double width;
  final PageController projectController;
  final List<Project> projects;

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout.builder(
      mobile: (BuildContext context) => Container(),
      desktop: (BuildContext context) => FooterWeb(
        width: this.width,
        height: this.height,
        projects: this.projects,
        projectController: this.projectController,
      ),
    );
  }
}