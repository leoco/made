import 'package:flutter/material.dart';
import 'package:made/Model/Project.dart';
import 'package:made/constants.dart';
import 'package:made/Tools/HoverExtensions.dart';

class FooterWeb extends StatefulWidget {
  FooterWeb(
      {Key key, this.height, this.width, this.projects, this.projectController})
      : super(key: key);

  final double height;
  final double width;
  final PageController projectController;
  final List<Project> projects;

  @override
  _FooterState createState() => _FooterState();
}

class _FooterState extends State<FooterWeb> {
  @override
  Widget build(BuildContext context) {
    final double totalWidth = (MediaQuery.of(context).size.width -
        2 * ProjectConstants.WEB_SOCIAL_WIDTH);

    final double fontSize = totalWidth /
        ProjectConstants.maxProjectFooterSize /
        ProjectConstants.projects.length /
        1.5;

    return Container(
      height: widget.height,
      width: widget.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        children: widget.projects
            .asMap()
            .map((index, project) {
              return MapEntry(
                index,
                _ProjectTitle(
                  index: index,
                  projectController: widget.projectController,
                  projectNumber: widget.projects.length,
                  name: project.name,
                  fontSize: fontSize,
                ),
              );
            })
            .values
            .toList(),
      ),
    );
  }
}

class _ProjectTitle extends StatefulWidget {
  _ProjectTitle(
      {Key key,
      this.index,
      this.projectNumber,
      this.projectController,
      this.name,
      this.fontSize})
      : super(key: key);

  final int index;
  final double fontSize;
  final String name;
  final int projectNumber;
  final PageController projectController;

  @override
  __ProjectTitleState createState() => __ProjectTitleState();
}

class __ProjectTitleState extends State<_ProjectTitle>
    with SingleTickerProviderStateMixin {
  AnimationController _animationUnderline;
  Animation<Decoration> _underlineDecoration;
  DecorationTween _underlineTween = DecorationTween(
    begin: UnderlineIndicator(percent: 0),
    end: UnderlineIndicator(percent: 1),
  );

  @override
  void initState() {
    super.initState();

    _animationUnderline = AnimationController(
      vsync: this,
      duration: Duration(seconds: 4),
    );
    _underlineDecoration = _underlineTween.animate(
      CurvedAnimation(
        parent: _animationUnderline,
        curve: Curves.easeInOut,
      ),
    );

    // Start the animation the first time the page is launched
    if (widget.index == 0) _animationUnderline.forward();

    // Set listener for current project title
    widget.projectController.addListener(() {
      if (widget.index ==
          widget.projectController.page % widget.projectNumber) {
        _animationUnderline.duration = Duration(milliseconds: 3500);
        _animationUnderline.forward();
      } else {
        _animationUnderline.duration = Duration(milliseconds: 200);
        _animationUnderline.reverse();
      }
    });
  }

  @override
  void dispose() {
    _animationUnderline.dispose();
    super.dispose();
  }

  void _onTap() {
    widget.projectController.animateToPage(
      100 * ProjectConstants.projects.length + widget.index,
      duration: Duration(
        milliseconds: 500,
      ),
      curve: Curves.easeOut,
    );
  }

  void _onHoverIn(PointerEvent e) {
    _animationUnderline.duration = Duration(milliseconds: 100);
    _animationUnderline.forward();
  }

  void _onHoverOut(PointerEvent e) {
    _animationUnderline.duration = Duration(milliseconds: 100);
    _animationUnderline.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: MouseRegion(
        onEnter: _onHoverIn,
        onExit: _onHoverOut,
        child: DecoratedBoxTransition(
          decoration: _underlineDecoration,
          child: Text(
            widget.name,
            style: TextStyle(
              fontFamily: 'MonumentExtended',
              fontSize: 14,
              fontWeight: FontWeight.w400,
            ),
          ),
        ),
      ).customCursorIsClickable,
    );
  }
}

class UnderlineIndicator extends Decoration {
  const UnderlineIndicator(
      {this.borderSide = const BorderSide(width: 2.0, color: Colors.black),
      this.insets = const EdgeInsets.only(bottom: -3),
      this.percent})
      : assert(borderSide != null),
        assert(insets != null);

  final BorderSide borderSide;
  final EdgeInsetsGeometry insets;
  final double percent;

  @override
  _UnderlineIndicator createBoxPainter([VoidCallback onChanged]) {
    return _UnderlineIndicator(this, onChanged, percent);
  }

  @override
  Decoration lerpTo(Decoration b, double t) {
    return UnderlineIndicator(
      borderSide: this.borderSide,
      insets: this.insets,
      percent: t,
    );
  }
}

class _UnderlineIndicator extends BoxPainter {
  _UnderlineIndicator(this.decoration, VoidCallback onChanged, this.percent)
      : assert(decoration != null),
        super(onChanged);

  final UnderlineIndicator decoration;
  final double percent;

  BorderSide get borderSide => decoration.borderSide;

  EdgeInsetsGeometry get insets => decoration.insets;

  Rect _indicatorRectFor(Rect rect, TextDirection textDirection) {
    assert(rect != null);
    assert(textDirection != null);
    final Rect indicator = insets.resolve(textDirection).deflateRect(rect);
    return Rect.fromLTWH(
      indicator.left,
      indicator.bottom - borderSide.width,
      indicator.width * percent,
      borderSide.width,
    );
  }

  @override
  void paint(Canvas canvas, Offset offset, ImageConfiguration configuration) {
    assert(configuration != null);
    assert(configuration.size != null);
    final Rect rect = offset & configuration.size;
    final TextDirection textDirection = configuration.textDirection;
    final Rect indicator = _indicatorRectFor(rect, textDirection).deflate(0);
    final Paint paint = borderSide.toPaint()..strokeCap = StrokeCap.butt;
    canvas.drawLine(indicator.bottomLeft, indicator.bottomRight, paint);
  }
}
