import 'package:async/async.dart';
import 'package:flutter/material.dart';

import 'package:made/constants.dart';
import 'package:made/View/ProjectList/Footer/Footer.dart';
import 'package:made/View/ProjectList/Carousel/ProjectCarousel.dart';

class ProjectListContainer extends StatefulWidget {
  @override
  _ProjectListContainerState createState() => _ProjectListContainerState();
}

class _ProjectListContainerState extends State<ProjectListContainer> {
  PageController _projectController;
  RestartableTimer _projectTimer;

  @override
  void initState() {
    super.initState();
    // Initialize the page controller at index, so we can scroll backward at the beginning
    _projectController = PageController(
      initialPage: ProjectConstants.projects.length * 100,
    );

    // Start the Slideshow
    _projectTimer = RestartableTimer(
      Duration(seconds: ProjectConstants.PROJECT_DURATION),
          () {
        // Next page only if carousel is on top
        if (ModalRoute.of(context).isCurrent) {
          _projectController.nextPage(
            duration: Duration(
                milliseconds: ProjectConstants.PROJECT_TRANSITION_DURATION),
            curve: Curves.easeInOut,
          );
        }
        _projectTimer.reset();
      },
    );
  }

  @override
  void dispose() {
    _projectController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        ProjectCarousel(
          projects: ProjectConstants.projects,
          projectController: _projectController,
          projectTimer: _projectTimer,
        ),
        Footer(
          width: MediaQuery.of(context).size.width -
              2 * ProjectConstants.WEB_SOCIAL_WIDTH,
          height: ProjectConstants.FOOTER_HEIGHT,
          projectController: _projectController,
          projects: ProjectConstants.projects,
        )
      ],
    );
  }
}
