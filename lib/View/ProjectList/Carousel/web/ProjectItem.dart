import 'package:flutter/material.dart';

import 'package:made/Model/Project.dart';
import 'package:made/View/utils/ClickButton.dart';
import 'package:made/constants.dart';
import 'package:made/tools/HoverExtensions.dart';
import 'package:made/tools/ParallaxExtensions.dart';

class ProjectItem extends StatefulWidget {
  ProjectItem({Key key, this.project, this.onTap}) : super(key: key);

  final Function onTap;
  final Project project;

  @override
  _ProjectItemState createState() => _ProjectItemState();
}

class _ProjectItemState extends State<ProjectItem>
    with SingleTickerProviderStateMixin {
  /// Controller for the animation
  AnimationController _controller;

  // Define each animations
  Animation<Offset> upperTextOffset;
  Animation<double> upperTextOpacity;
  Animation<Offset> bottomTextOffset;
  Animation<double> bottomTextOpacity;
  Animation<Offset> descriptionOffset;
  Animation<double> descriptionOpacity;
  Animation<Offset> bgOffset;
  Animation<double> bgOpacity;
  Animation<double> colorWidth;
  Animation<double> colorOpacity;

  /// Initialize each animations
  @override
  void initState() {
    super.initState();

    // Create the controller
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 2500),
    );

    this.upperTextOffset = Tween<Offset>(
      begin: Offset(-40, 0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(
        0.25,
        0.5,
        curve: Curves.easeOut,
      ),
    ));
    this.upperTextOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.25,
          0.5,
          curve: Curves.easeOut,
        ),
      ),
    );

    this.bottomTextOffset = Tween<Offset>(
      begin: Offset(40, 0),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(
        0.25,
        0.5,
        curve: Curves.easeOut,
      ),
    ));
    this.bottomTextOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.25,
          0.5,
          curve: Curves.easeOut,
        ),
      ),
    );

    this.descriptionOffset = Tween<Offset>(
      begin: Offset(0, 80),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(
        0.4,
        0.65,
        curve: Curves.easeOut,
      ),
    ));
    this.descriptionOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.4,
          0.65,
          curve: Curves.easeOut,
        ),
      ),
    );

    this.bgOffset = Tween<Offset>(
      begin: Offset(0, 40),
      end: Offset.zero,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(
        0.55,
        0.80,
        curve: Curves.easeOut,
      ),
    ));
    this.bgOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.55,
          0.80,
          curve: Curves.easeOut,
        ),
      ),
    );

    this.colorWidth = Tween<double>(
      begin: 0,
      end: 1,
    ).animate(CurvedAnimation(
      parent: _controller,
      curve: Interval(
        0.25,
        0.4,
        curve: Curves.easeOut,
      ),
    ));
    this.colorOpacity = Tween<double>(
      begin: 0.0,
      end: 1.0,
    ).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Interval(
          0.25,
          0.5,
          curve: Curves.easeOut,
        ),
      ),
    );

    // Start the animation
    _controller.forward();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final double totalWidth = (MediaQuery.of(context).size.width -
        2 * ProjectConstants.WEB_SOCIAL_WIDTH);

    return Row(
      children: <Widget>[
        // First column with text and description
        Container(
          width: totalWidth / 2,
          padding: const EdgeInsets.only(
            top: 100,
            left: 50,
          ),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              AnimatedBuilder(
                animation: _controller,
                builder: (BuildContext context, Widget child) => Opacity(
                  opacity: upperTextOpacity.value,
                  child: Transform.translate(
                    offset: upperTextOffset.value,
                    child: child,
                  ),
                ),
                child: Hero(
                  tag: widget.project.upperText,
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      widget.project.upperText,
                      style: TextStyle(
                        fontFamily: 'MonumentExtended',
                        fontSize: 52,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
              ),
              widget.project.bottomText != null
                  ? FittedBox(
                      fit: BoxFit.contain,
                      child: AnimatedBuilder(
                        animation: _controller,
                        builder: (context, child) => Opacity(
                          opacity: bottomTextOpacity.value,
                          child: Transform.translate(
                            offset: bottomTextOffset.value,
                            child: child,
                          ),
                        ),
                        child: Row(
                          children: <Widget>[
                            Text(
                              '-',
                              style: TextStyle(
                                fontFamily: 'MonumentExtended',
                                fontSize: 52,
                                fontWeight: FontWeight.w400,
                              ),
                            ),
                            Hero(
                              tag: widget.project.bottomText,
                              child: FittedBox(
                                fit: BoxFit.contain,
                                child: Text(
                                  " " + widget.project.bottomText,
                                  style: TextStyle(
                                    fontFamily: 'MonumentExtended',
                                    fontSize: 52,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            )
                          ],
                        ),
                      ),
                    )
                  : Container(),
              Expanded(
                child: AnimatedBuilder(
                  animation: _controller,
                  builder: (context, child) => Opacity(
                    opacity: descriptionOpacity.value,
                    child: Transform.translate(
                      offset: descriptionOffset.value,
                      child: child,
                    ),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.only(top: 50),
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text(
                          widget.project.description,
                          textAlign: TextAlign.start,
                          style: TextStyle(
                            fontSize: 27,
                            fontWeight: FontWeight.w400,
                            letterSpacing: 1.6,
                          ),
                        ),
                        SizedBox(height: 20),
                        ClickButton(onTap: widget.onTap),
                      ],
                    ),
                  ),
                ),
              ),
            ],
          ),
        ),

        // Second column with image and background
        Container(
          width: totalWidth / 2,
          child: Stack(
            alignment: Alignment.centerRight,
            children: <Widget>[
              AnimatedBuilder(
                animation: _controller,
                builder: (context, child) => Opacity(
                  opacity: colorOpacity.value,
                  child: Container(
                    alignment: Alignment.centerRight,
                    color: widget.project.bgColor,
                    width: 0.8 * totalWidth / 2 * colorWidth.value,
                  ),
                ),
              ),
              AnimatedBuilder(
                animation: _controller,
                builder: (context, child) => Opacity(
                  opacity: bgOpacity.value,
                  child: Transform.translate(
                    offset: bgOffset.value,
                    child: child,
                  ),
                ),
                child: Align(
                  alignment: Alignment.centerLeft,
                  child: Hero(
                    tag: widget.project.name,
                    child: Image.asset(
                      widget.project.bgImage,
                      fit: BoxFit.fill,
                      semanticLabel: "Image for ${widget.project.name}",
                    ).withMouseParallax(
                      invertX: true,
                      invertY: true,
                    ),
                  ),
                ),
              ),
            ],
          ),
        )
      ],
    ).customCursorIsClickable;
  }
}
