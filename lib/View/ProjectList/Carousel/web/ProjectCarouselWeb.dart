import 'package:flutter/material.dart';
import 'package:made/Model/Project.dart';
import 'package:made/constants.dart';
import 'package:made/routes.dart';
import 'package:async/async.dart';

import 'ProjectItem.dart';

class ProjectCarouselWeb extends StatelessWidget {
  ProjectCarouselWeb({
    Key key,
    this.projects,
    this.projectController,
    this.projectTimer,
  }) : super(key: key);

  final PageController projectController;
  final RestartableTimer projectTimer;
  final List<Project> projects;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: MediaQuery.of(context).size.height -
          ProjectConstants.HEADER_HEIGHT -
          ProjectConstants.FOOTER_HEIGHT,
      child: PageView.builder(
        controller: projectController,
        physics: BouncingScrollPhysics(),
        onPageChanged: (_) => projectTimer.reset(),
        itemBuilder: (BuildContext context, int index) => InkWell(
          focusColor: Colors.transparent,
          hoverColor: Colors.transparent,
          splashColor: Colors.transparent,
          highlightColor: Colors.transparent,
          onTap: () => ProjectRoutes.navigatorKey.currentState
              .pushNamed('projects/${index % projects.length}'),
          child: ProjectItem(
            project: projects[index % projects.length],
            onTap: () => ProjectRoutes.navigatorKey.currentState
                .pushNamed('projects/${index % projects.length}'),
          ),
        ),
      ),
    );
  }
}