import 'package:async/async.dart';
import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'web/ProjectCarouselWeb.dart';
import 'package:made/Model/Project.dart';


class ProjectCarousel extends StatelessWidget {

  ProjectCarousel({
    Key key,
    this.projects,
    this.projectController,
    this.projectTimer,
  }) : super(key: key);

  final PageController projectController;
  final RestartableTimer projectTimer;
  final List<Project> projects;

  @override
  Widget build(BuildContext context) => ScreenTypeLayout.builder(
    mobile: (BuildContext context) => Container(),
    desktop: (BuildContext context) => ProjectCarouselWeb(
      projects: this.projects,
      projectTimer: this.projectTimer,
      projectController: this.projectController,
    ),
  );
}