import 'package:flutter/material.dart';
import 'package:made/View/About/components/TmpScreen.dart';
import 'package:made/View/about/components/FifthScreen.dart';

import 'package:made/View/about/components/FirstScreen.dart';
import 'package:made/View/about/components/SecondScreen.dart';
import 'package:made/View/about/components/ThirdScreen.dart';
import 'package:made/View/about/components/FourthScreen.dart';
import 'package:made/tools/HoverExtensions.dart';

class About extends StatelessWidget {
  final screens = [
    FirstScreen(),
    //AboutVideo(),
    ThirdScreen(),
    TmpScreen(),
    FourthScreen(),
    FifthScreen(),
  ];

  @override
  Widget build(BuildContext context) {
    return Material(
      child: ListView.builder(
        itemCount: screens.length,
        itemBuilder: (BuildContext context, int index) => screens[index],
      ),
    ).showCustomCursorOnHover;
  }
}
