import 'package:flutter/material.dart';

import 'package:made/View/leftPanel/web/ContactForm.dart';
import 'package:made/tools/HoverExtensions.dart';

class FourthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final padding = size.width / 10;
    return SizedBox(
      height: size.height / 2,
      child: Container(
        color: Colors.black,
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(bottom: 80.0, left: padding),
              child: Text(
                "Je suis la personne qu'il vous faut si...",
                textAlign: TextAlign.left,
                style: TextStyle(
                  fontFamily: 'MonumentExtended',
                  fontWeight: FontWeight.normal,
                  fontSize: 28,
                  color: Colors.white,
                ),
              ),
            ),
            Padding(
              padding: EdgeInsets.symmetric(horizontal: padding),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "Vous recherchez un\naccompagnement\npersonnalisé et non une\ngraphiste exécutante",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "Vous comprenez\nl'importance d'avoir une\nimage de marque unique\net qui parle à votre cible",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 20),
                    child: Text(
                      "Vous êtes motivé,\ndéterminé et prêt à vous\ndémarquer",
                      textAlign: TextAlign.left,
                      style: TextStyle(
                        fontWeight: FontWeight.w800,
                        fontSize: 18,
                        color: Colors.white,
                      ),
                    ),
                  ),
                ],
              ),
            ),
            Align(
              alignment: Alignment.centerRight,
              child: Padding(
                padding: EdgeInsets.only(top: 80, right: padding),
                child: GestureDetector(
                  onTap: () => showDialog(
                    context: context,
                    builder: (context) => ContactForm(),
                  ),
                  child: Text(
                    "Contacter le studio",
                    style: TextStyle(
                      color: Colors.white,
                      fontFamily: "MonumentExtended",
                      fontSize: 20,
                      fontWeight: FontWeight.w400,
                      decoration: TextDecoration.underline,
                    ),
                  ).customCursorIsClickable,
                ),
              ),
            )
          ],
        ),
      ),
    );
  }
}
