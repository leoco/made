import 'dart:html' as html;
import 'dart:ui';

import 'package:async/async.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:made/View/leftPanel/web/ContactForm.dart';
import 'package:made/constants.dart';

import 'package:made/tools/HoverExtensions.dart';

class FifthScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    final GestureRecognizer behanceClick = TapGestureRecognizer()
      ..onTap = () => html.window.open(
            "https://www.behance.net/madissonrolland_made/",
            "Lien Behance",
          );
    final GestureRecognizer instaClick = TapGestureRecognizer()
      ..onTap = () => html.window.open(
            "https://www.instagram.com/madisson_rolland/",
            "Lien Instagram",
          );
    return Padding(
      padding: const EdgeInsets.only(top: 100.0),
      child: Column(
        children: <Widget>[
          FittedBox(
            fit: BoxFit.contain,
            child: Text(
              "Références",
              style: TextStyle(
                fontFamily: "MonumentExtended",
                fontSize: 100,
                fontWeight: FontWeight.w800,
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40, bottom: 200),
            child: References(),
          ),
          FittedBox(
            fit: BoxFit.contain,
            child: RichText(
              textAlign: TextAlign.center,
              text: TextSpan(
                style: TextStyle(
                  fontFamily: "MonumentExtended",
                  fontSize: 45,
                  fontWeight: FontWeight.w400,
                  color: Colors.black,
                ),
                children: [
                  TextSpan(
                      text:
                          "Vous pouvez regardez l’ensemble\n de mes créations sur "),
                  TextSpan(
                    text: "behance\n",
                    recognizer: behanceClick,
                    style: TextStyle(
                      fontFamily: "MonumentExtended",
                      fontSize: 45,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFFFF0000),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                  TextSpan(text: "ou encore sur "),
                  TextSpan(
                    text: "Instagram",
                    recognizer: instaClick,
                    style: TextStyle(
                      fontFamily: "MonumentExtended",
                      fontSize: 45,
                      fontWeight: FontWeight.w400,
                      color: Color(0xFFFF0000),
                      decoration: TextDecoration.underline,
                    ),
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 200),
            child: SizedBox(
              width: size.width,
              height: size.height * 0.7,
              child: Stack(
                fit: StackFit.expand,
                children: [
                  RepaintBoundary(
                    child: Image.asset(
                      "assets/forme-rouge-big.png",
                      fit: BoxFit.cover,
                      alignment: Alignment.topCenter,
                    ),
                  ),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      FittedBox(
                        fit: BoxFit.contain,
                        child: Text(
                          "un projet, une idée ?",
                          style: TextStyle(
                            color: Colors.white,
                            fontFamily: "MonumentExtended",
                            fontSize: 70,
                            fontWeight: FontWeight.w800,
                          ),
                        ),
                      ),
                      FittedBox(
                        fit: BoxFit.contain,
                        child: GestureDetector(
                          onTap: () => showDialog(
                            context: context,
                            builder: (context) => ContactForm(),
                          ),
                          child: Text(
                            "contactez-moi",
                            style: TextStyle(
                              color: Colors.white,
                              fontFamily: "MonumentExtended",
                              fontSize: 70,
                              fontWeight: FontWeight.w800,
                              decoration: TextDecoration.underline,
                            ),
                          ),
                        ),
                      ).customCursorIsClickable,
                    ],
                  )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

class References extends StatefulWidget {
  @override
  _ReferencesState createState() => _ReferencesState();
}

class _ReferencesState extends State<References> {
  final _refs = ProjectConstants.references;
  final _scrollController = ScrollController();
  final _duration = Duration(milliseconds: 500);

  RestartableTimer _projectTimer;
  double _offset = 100;

  @override
  void initState() {
    super.initState();
    _projectTimer = RestartableTimer(_duration, () {
      _offset += 25;
      _scrollController.animateTo(
        _offset,
        duration: _duration,
        curve: Curves.linear,
      );
      _projectTimer.reset();
    });
  }

  @override
  void dispose() {
    _projectTimer.cancel();
    _scrollController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width,
      height: 20,
      child: ListView.builder(
        controller: _scrollController,
        physics: NeverScrollableScrollPhysics(),
        scrollDirection: Axis.horizontal,
        itemBuilder: (context, index) => Padding(
          padding: const EdgeInsets.only(left: 40),
          child: Text(
            _refs[index % _refs.length],
            style: TextStyle(
              fontFamily: "MonumentExtended",
              fontSize: 18,
              color: Color(0xFFFF0000),
            ),
          ),
        ),
      ),
    );
  }
}
