import 'package:async/async.dart';
import 'package:flutter/material.dart';

class ScrollDown extends StatefulWidget {
  @override
  _ScrollDownState createState() => _ScrollDownState();
}

class _ScrollDownState extends State<ScrollDown>
    with SingleTickerProviderStateMixin {
  RestartableTimer sizeTimer;
  double size = 0.0;
  Alignment alignment = Alignment.bottomCenter;

  @override
  void initState() {
    super.initState();

    sizeTimer = RestartableTimer(
      Duration(milliseconds: 1000),
      () {
        setState(() {
          size = (size + 40.0) % 80.0;
          alignment = Alignment.lerp(
              Alignment.bottomCenter, Alignment.topCenter, size / 40.0);
        });
        sizeTimer.reset();
      },
    );
  }

  @override
  void dispose() {
    sizeTimer.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 100,
      height: 100,
      child: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Text(
              "Scroll down",
              style: TextStyle(
                fontWeight: FontWeight.w800,
                color: Color(0xFFA7A7A7),
              ),
            ),
            SizedBox(height: 10),
            SizedBox(
              height: 40,
              child: Align(
                alignment: alignment,
                child: AnimatedContainer(
                  curve: Curves.easeInOut,
                  duration: Duration(
                    milliseconds: 1000,
                  ),
                  width: 2,
                  height: size,
                  color: Color(0xFFA7A7A7),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
