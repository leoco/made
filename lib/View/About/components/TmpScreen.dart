import 'dart:html' as html;

import 'package:flutter/material.dart';

import 'package:made/tools/HoverExtensions.dart';

class TmpScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Image.asset('assets/other/uxui_cards.png', fit: BoxFit.fitWidth,),
        Image.asset('assets/other/studio_vision.png', fit: BoxFit.fitWidth,),
        Image.asset('assets/other/demarche.png', fit: BoxFit.fitWidth,),
        Image.asset('assets/other/domaines.png', fit: BoxFit.fitWidth,),
      ],
    );
  }
}
