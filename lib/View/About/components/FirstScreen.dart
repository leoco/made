import 'package:flutter/material.dart';

import 'package:made/View/about/components/ScrollDown.dart';
import 'package:made/View/header/Header.dart';
import 'package:made/View/leftPanel/LeftPanel.dart';
import 'package:made/constants.dart';
import 'package:made/tools/parallaxExtensions.dart';

class FirstScreen extends StatelessWidget {
  static const _redFactorHeight = 0.544 * 1.4;
  static const _redFactorWidth = 0.243 * 1.4;

  @override
  Widget build(BuildContext context) {
    Size size = MediaQuery.of(context).size;
    return SizedBox(
      height: size.height,
      child: Stack(
        children: <Widget>[
          Positioned(
            bottom: -size.height,
            right: -size.width / 2,
            child: Transform.rotate(
              angle: 180,
              child: Image.asset(
                "assets/forme-jaune.png",
                fit: BoxFit.contain,
              ).withMouseParallax(factor: 0.01, invertX: false, invertY: false),
            ),
          ),
          Positioned(
            top: -size.height / 3,
            right: size.width / 3,
            height: size.height * _redFactorHeight,
            width: size.width * _redFactorWidth,
            child: Image.asset(
              "assets/forme-rouge.png",
              fit: BoxFit.contain,
            ).withMouseParallax(factor: 0.01, invertX: true, invertY: false),
          ),
          Header(
            isFromAbout: true,
          ),
          Padding(
            padding:
                const EdgeInsets.only(top: ProjectConstants.HEADER_HEIGHT),
            child: LeftPanel(),
          ),
          Center(
            child: FractionallySizedBox(
              widthFactor: 9 / 12,
              heightFactor: 2 / 3,
              child: FittedBox(
                fit: BoxFit.contain,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
                    Text(
                      "madisson rolland\ndirection artistique\nux/ui design\ngraphisme",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontFamily: 'MonumentExtended',
                        fontWeight: FontWeight.w800,
                        height: 1,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Padding(
              padding: const EdgeInsets.only(bottom: 20.0),
              child: ScrollDown(),
            ),
          )
        ],
      ),
    );
  }
}
