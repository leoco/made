import 'dart:html' as html;

import 'package:flutter/material.dart';

import 'package:made/tools/parallaxExtensions.dart';
import 'package:made/tools/HoverExtensions.dart';

class ThirdScreen extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 100),
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(bottom: 100),
            child: Image.asset('assets/other/voyage.png'),
          ),
          FractionallySizedBox(
            widthFactor: 88 / 120,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Text(
                "Qui se cache derrière mad(e) ?",
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontFamily: "MonumentExtended",
                  fontWeight: FontWeight.w800,
                ),
              ),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 70 / 120,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Text(
                  "Hello, moi c’est Madisson, allias Mad.\nJe suis graphiste et UI designer sur Lyon depuis\n2016. Je créée des design intelligents pour servir\nles entreprises, marques, startup, grands\ngroupes ou personnes.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: "MonumentExtended",
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 70 / 120,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Text(
                  "Ma mission première, avant la création, est de\ntrouver les solutions qui vont mettre votre projet\nen valeur et impacter positivement votre business.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: "MonumentExtended",
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 70 / 120,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Text(
                  "Parce qu’un design sensé est plus fort qu’un design\nesthétique.",
                  textAlign: TextAlign.center,
                  style: TextStyle(
                    fontFamily: "MonumentExtended",
                    fontWeight: FontWeight.w400,
                    fontSize: 22,
                  ),
                ),
              ),
            ),
          ),
          FractionallySizedBox(
            widthFactor: 32 / 120,
            child: FittedBox(
              fit: BoxFit.contain,
              child: Padding(
                padding: const EdgeInsets.only(top: 30.0),
                child: Row(
                  children: <Widget>[
                    GestureDetector(
                      onTap: () => html.window.open(
                        "https://www.linkedin.com/in/madisson-rolland-a02229129/",
                        "Lien linkedin",
                      ),
                      child: Text(
                        "Linkedin",
                        style: TextStyle(
                          fontFamily: "MonumentExtended",
                          decoration: TextDecoration.underline,
                        ),
                      ).customCursorIsClickable,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 40.0),
                      child: GestureDetector(
                        onTap: () => html.window.open(
                          "https://www.madissonrolland.fr/CV.pdf",
                          "Lien CV",
                        ),
                        child: Text(
                          "Voir mon CV",
                          style: TextStyle(
                            fontFamily: "MonumentExtended",
                            decoration: TextDecoration.underline,
                          ),
                        ).customCursorIsClickable,
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
