import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:made/View/header/mobile/HeaderMobile.dart';
import 'package:made/View/header/web/HeaderWeb.dart';

class Header extends StatelessWidget {
  Header({bool isFromAbout}) : _isFromAbout = isFromAbout ?? false;

  final bool _isFromAbout;

  @override
  Widget build(BuildContext context) => Semantics(
    header: true,
    child: ScreenTypeLayout.builder(
          mobile: (BuildContext context) => HeaderMobile(
            isFromAbout: _isFromAbout,
          ),
          desktop: (BuildContext context) => HeaderWeb(
            isFromAbout: _isFromAbout,
          ),
        ),
  );
}
