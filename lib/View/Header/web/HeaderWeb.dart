import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

import 'package:made/constants.dart';
import 'package:made/routes.dart';
import 'package:made/tools/HoverExtensions.dart';

class HeaderWeb extends StatelessWidget {
  HeaderWeb({bool isFromAbout}) : _isFromAbout = isFromAbout ?? false;

  final bool _isFromAbout;

  void _onTapProjects(BuildContext context) {
    if (!_isFromAbout) {
      ProjectRoutes.navigatorKey.currentState.pushReplacementNamed('projects');
    } else {
      Navigator.of(context, rootNavigator: true)
          .pushReplacementNamed('/fromAbout');
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: ProjectConstants.HEADER_HEIGHT,
      child: Padding(
        padding: EdgeInsets.symmetric(
            horizontal: ProjectConstants.WEB_SOCIAL_WIDTH, vertical: 0.0),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            GestureDetector(
              onTap: () => _onTapProjects(context),
              child: SizedBox(
                height: 40,
                child: Image.asset(
                  'assets/made.png',
                  semanticLabel: "Logo Mad(e)",
                ),
              ).customCursorIsClickable,
            ),
            Spacer(),
            Padding(
              padding: const EdgeInsets.only(right: 30),
              child: GestureDetector(
                onTap: () => _onTapProjects(context),
                child: Text(
                  "projets",
                  style: TextStyle(
                    fontFamily: 'MonumentExtended',
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ).customCursorIsClickable,
              ),
            ),
            Padding(
              padding: const EdgeInsets.only(right: 30),
              child: GestureDetector(
                onTap: () => ProjectRoutes.appRouter.navigateTo(
                  context,
                  '/digital',
                  transition: TransitionType.custom,
                  transitionBuilder: ProjectRoutes.transitionBuilder,
                  transitionDuration: Duration(
                    milliseconds:
                    ProjectConstants.TRANSITION_BETWEEN_SCREENS_DURATION,
                  ),
                ),
                child: Text(
                  "illustrations",
                  style: TextStyle(
                    fontFamily: 'MonumentExtended',
                    fontWeight: FontWeight.w400,
                    fontSize: 14,
                  ),
                ).customCursorIsClickable,
              ),
            ),
            GestureDetector(
              onTap: () => ProjectRoutes.appRouter.navigateTo(
                context,
                '/about',
                transition: TransitionType.custom,
                transitionBuilder: ProjectRoutes.transitionBuilder,
                transitionDuration: Duration(
                  milliseconds:
                      ProjectConstants.TRANSITION_BETWEEN_SCREENS_DURATION,
                ),
              ),
              child: Text(
                'à propos',
                style: TextStyle(
                  fontFamily: 'MonumentExtended',
                  fontWeight: FontWeight.w400,
                  fontSize: 14,
                ),
              ).customCursorIsClickable,
            ),
          ],
        ),
      ),
    );
  }
}
