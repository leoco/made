import 'package:flutter/material.dart';
import 'package:made/constants.dart';
import 'package:made/routes.dart';

class HeaderMobile extends StatefulWidget {
  HeaderMobile({bool isFromAbout}) : _isFromAbout = isFromAbout ?? false;

  final bool _isFromAbout;

  @override
  _HeaderMobileState createState() => _HeaderMobileState();
}

class _HeaderMobileState extends State<HeaderMobile>
    with SingleTickerProviderStateMixin {
  AnimationController _controller;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(
        milliseconds: 200,
      ),
    );
  }

  void _onTapProjects(BuildContext context) {
    if (!widget._isFromAbout) {
      ProjectRoutes.navigatorKey.currentState.pushReplacementNamed('projects');
    } else {
      Navigator.of(context, rootNavigator: true)
          .pushReplacementNamed('/fromAbout');
    }
  }

  void _onTapMenu() {
    _controller.forward();
  }

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return Container(
      height: ProjectConstants.HEADER_HEIGHT,
      padding: const EdgeInsets.symmetric(horizontal: 30),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          GestureDetector(
            onTap: () => _onTapProjects(context),
            child: SizedBox(
              width: screenSize.width / 3,
              child: Image.asset(
                'assets/made.png',
                semanticLabel: "Logo Mad(e)",
              ),
            ),
          ),
//          GestureDetector(
//            onTap: _onTapMenu,
//            child: AnimatedIcon(
//              icon: AnimatedIcons.menu_close,
//              progress: _controller,
//            ),
//          )
        ],
      ),
    );
  }
}
