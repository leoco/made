import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart';

import 'package:made/routes.dart';
import 'package:made/constants.dart';
import 'package:made/View/header/Header.dart';
import 'package:made/View/leftPanel/LeftPanel.dart';
import 'package:made/Tools/HoverExtensions.dart';


class MainContainer extends StatelessWidget {
  MainContainer({this.initialRoute = 'home'});

  final String initialRoute;

  @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    return Column(
      crossAxisAlignment: CrossAxisAlignment.stretch,
      children: <Widget>[
        Header(),
        Row(
          textDirection: TextDirection.rtl,
          children: [
            Expanded(
              child: SizedBox(
                height: size.height - ProjectConstants.HEADER_HEIGHT,
                child: WillPopScope(
                  onWillPop: () async =>
                      !await ProjectRoutes.navigatorKey.currentState.maybePop(),
                  child: Navigator(
                    key: ProjectRoutes.navigatorKey,
                    observers: [HeroController()],
                    initialRoute: initialRoute,
                    onGenerateRoute: (RouteSettings settings) {
                      RouteMatch match = ProjectRoutes.embeddedRouter.matchRoute(
                        context,
                        settings.name,
                        routeSettings: settings,
                        transitionType: TransitionType.custom,
                        transitionDuration: Duration(
                          milliseconds: ProjectConstants
                              .TRANSITION_BETWEEN_SCREENS_DURATION,
                        ),
                        transitionsBuilder: ProjectRoutes.transitionBuilder,
                      );
                      return match.route;
                    },
                  ),
                ),
              ),
            ),
            LeftPanel(),
          ],
        ),
      ],
    ).showCustomCursorOnHover;
  }
}
