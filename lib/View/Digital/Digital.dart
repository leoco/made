import 'package:flutter/material.dart';

import 'package:made/View/Header/Header.dart';
import 'package:made/View/LeftPanel/LeftPanel.dart';
import 'package:made/constants.dart';
import 'package:made/main.dart';

class Digital extends StatefulWidget {
  @override
  _DigitalState createState() => _DigitalState();
}

class _DigitalState extends State<Digital> {
  String _filter = "All";

  void showImage(String name, String description) {
    showDialog(
      context: context,
      builder: (_) => Padding(
        padding: const EdgeInsets.all(30.0),
        child: Stack(
          alignment: Alignment.center,
          fit: StackFit.loose,
          children: [
            Card(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Expanded(
                    child: Image.asset(
                      name,
                      fit: BoxFit.contain,
                      filterQuality: FilterQuality.high,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(10),
                    child: Text(
                      description,
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: Column(
        children: [
          Header(isFromAbout: true),
          Row(
            children: [
              LeftPanel(),
              SizedBox(
                height: MediaQuery.of(context).size.height -
                    ProjectConstants.HEADER_HEIGHT,
                width: MediaQuery.of(context).size.width -
                    ProjectConstants.WEB_SOCIAL_WIDTH,
                child: Padding(
                  padding: const EdgeInsets.only(
                    right: ProjectConstants.WEB_SOCIAL_WIDTH,
                    bottom: 20,
                  ),
                  child: CustomScrollView(
                    primary: true,
                    slivers: [
                      SliverAppBar(
                        automaticallyImplyLeading: false,
                        pinned: true,
                        floating: true,
                        snap: false,
                        backgroundColor: Colors.white,
                        shadowColor: Colors.transparent,
                        flexibleSpace: Wrap(
                          spacing: 10,
                          alignment: WrapAlignment.end,
                          children: [
                            ChoiceChip(
                              label: Text(
                                "All",
                                style: TextStyle(
                                  color: _filter == "All"
                                      ? Colors.white
                                      : Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              selected: _filter == "All",
                              onSelected: (_) => setState(() {
                                _filter = "All";
                              }),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                            ChoiceChip(
                              label: Text(
                                "Digital",
                                style: TextStyle(
                                  color: _filter == "Digital"
                                      ? Colors.white
                                      : Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              selected: _filter == "Digital",
                              onSelected: (_) => setState(() {
                                _filter = "Digital";
                              }),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                            ChoiceChip(
                              label: Text(
                                "Photo",
                                style: TextStyle(
                                  color: _filter == "Photo"
                                      ? Colors.white
                                      : Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              selected: _filter == "Photo",
                              onSelected: (_) => setState(() {
                                _filter = "Photo";
                              }),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                            ChoiceChip(
                              label: Text(
                                "Traditionnelle",
                                style: TextStyle(
                                  color: _filter == "tradi"
                                      ? Colors.white
                                      : Colors.black,
                                  fontSize: 18,
                                ),
                              ),
                              selected: _filter == "tradi",
                              onSelected: (_) => setState(() {
                                _filter = "tradi";
                              }),
                              backgroundColor: Colors.white,
                              selectedColor: Colors.black,
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20),
                                side: BorderSide(
                                  color: Colors.black,
                                  width: 1.0,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SliverGrid.count(
                        crossAxisCount: 4,
                        mainAxisSpacing: 20,
                        crossAxisSpacing: 20,
                        children: ProjectConstants.illustrations
                            .where((illu) =>
                                illu.tags.contains(_filter) || _filter == "All")
                            .map(
                              (illu) => GestureDetector(
                                onTap: () =>
                                    showImage(illu.asset, illu.description),
                                child: Image.asset(
                                  illu.asset,
                                  fit: BoxFit.cover,
                                  semanticLabel: illu.name,
                                  filterQuality: FilterQuality.none,
                                ),
                              ),
                            )
                            .toList(),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          )
        ],
      ),
    );
  }
}
