import 'dart:math';
import 'package:flutter/material.dart';

import 'package:made/constants.dart';
import 'package:made/tools/HoverExtensions.dart';
import 'package:made/View/leftPanel/web/ContactForm.dart';

class Contact extends StatefulWidget {
  @override
  _ContactState createState() => _ContactState();
}

class _ContactState extends State<Contact> with TickerProviderStateMixin {
  AnimationController _rotationController;
  Animation<double> _textRotation;

  @override
  void initState() {
    super.initState();

    _rotationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );
    _textRotation = Tween<double>(
      begin: 0.0,
      end: 2 * pi,
    ).animate(_rotationController);

    _rotationController.addStatusListener((status) {
      if (status == AnimationStatus.completed) {
        _rotationController.reset();
        _rotationController.forward();
      }
    });
    _rotationController.forward();
  }

  @override
  void dispose() {
    _rotationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () =>
          showDialog(context: context, builder: (context) => ContactForm()),
      child: SizedBox(
        width: ProjectConstants.WEB_SOCIAL_WIDTH,
        height: ProjectConstants.FOOTER_HEIGHT,
        child: Container(
          child: AnimatedBuilder(
            animation: _textRotation,
            builder: (context, child) => Transform.rotate(
              angle: _textRotation.value,
              child: child,
            ),
            child: Transform.scale(
              scale: 0.8,
              child: Image.asset(
                'assets/contactez-moi.png',
                fit: BoxFit.contain,
              ),
            ),
          ),
        ),
      ).customCursorIsClickable,
    );
  }
}
