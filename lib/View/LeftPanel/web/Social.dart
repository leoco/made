import 'package:flutter/material.dart';
import 'package:made/constants.dart';

import 'package:made/tools/HoverExtensions.dart';

class Social extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      width: ProjectConstants.WEB_SOCIAL_WIDTH,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: ProjectConstants
            .socialLink
            .map(
              (textIcon) => Padding(
                padding: EdgeInsets.symmetric(vertical: 8),
                child: RotatedBox(
                  quarterTurns: 3,
                  child: textIcon.customCursorIsClickable,
                ),
              ),
            )
            .toList(),
      ),
    );
  }
}
