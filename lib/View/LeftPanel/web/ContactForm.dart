import 'package:flutter/material.dart';
import 'package:cloud_functions/cloud_functions.dart';

import 'package:made/tools/HoverExtensions.dart';

class ContactForm extends StatefulWidget {
  final formKey = GlobalKey<FormState>();
  final String regex =
      r"^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,253}[a-zA-Z0-9])?)*$";
  final HttpsCallable callable = FirebaseFunctions.instance.httpsCallable(
    'sendMail',
    options: HttpsCallableOptions(
      timeout: const Duration(seconds: 30),
    ),
  );

  final nameController = TextEditingController();
  final mailController = TextEditingController();
  final subjectController = TextEditingController();
  final messageController = TextEditingController();

  @override
  _ContactFormState createState() => _ContactFormState();
}

class _ContactFormState extends State<ContactForm> {
  bool isLoading = false;

  void sendEmail(BuildContext context) async {
    setState(() {
      isLoading = true;
    });
    try {
      await widget.callable.call(
        <String, dynamic>{
          'message': widget.messageController.value.text,
          'from': widget.mailController.value.text,
          'subject': widget.subjectController.value.text,
          'name': widget.nameController.value.text,
        },
      );
    } finally {
      setState(() {
        isLoading = false;
      });
      Navigator.of(context).pop();
    }
  }

  @override
  Widget build(BuildContext context) {
    return SimpleDialog(
      contentPadding: EdgeInsets.all(50),
      children: <Widget>[
        Center(
          child: Padding(
            padding: EdgeInsets.only(bottom: 50),
            child: Text(
              'Gardons le contact !',
              style: TextStyle(
                fontSize: 36,
                fontWeight: FontWeight.w400,
              ),
            ),
          ),
        ),
        SizedBox(
          width: MediaQuery.of(context).size.width / 2,
          child: Form(
            key: widget.formKey,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  children: <Widget>[
                    Expanded(
                      child: TextFormField(
                        controller: widget.nameController,
                        decoration: InputDecoration(
                          hintText: 'Votre Nom',
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return 'Je suis sur que vous avez au moins un surnom';
                          }
                          return null;
                        },
                      ),
                    ),
                    Expanded(
                      child: Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        child: TextFormField(
                          controller: widget.mailController,
                          keyboardType: TextInputType.emailAddress,
                          decoration: InputDecoration(
                            hintText: 'Votre Email',
                          ),
                          validator: (value) {
                            if (!RegExp(widget.regex).hasMatch(value)) {
                              return 'Email incorrect ?';
                            }
                            return null;
                          },
                        ),
                      ),
                    ),
                    Expanded(
                      child: TextFormField(
                        controller: widget.subjectController,
                        decoration: InputDecoration(
                          hintText: "Sujet",
                        ),
                        validator: (value) {
                          if (value.isEmpty) {
                            return "N'aillez pas peur d'écrire quelque chose :)";
                          }
                          return null;
                        },
                      ),
                    ),
                  ],
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: TextFormField(
                    controller: widget.messageController,
                    minLines: 10,
                    maxLines: 10,
                    decoration: InputDecoration(
                      hintText: "Ce qui vous passe par la tête !",
                    ),
                    validator: (value) {
                      if (value.isEmpty) {
                        return "N'aillez pas peur d'écrire quelque chose :)";
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: EdgeInsets.only(top: 50),
                  child: FlatButton(
                    padding: EdgeInsets.symmetric(
                      horizontal: 50,
                      vertical: 20,
                    ),
                    color: Colors.black,
                    textColor: Colors.white,
                    onPressed: () {
                      if (widget.formKey.currentState.validate()) {
                        sendEmail(context);
                      }
                    },
                    child: isLoading
                        ? CircularProgressIndicator(
                            backgroundColor: Colors.black,
                          )
                        : Text(
                            'Envoyer',
                            style: TextStyle(
                              fontSize: 16,
                            ),
                          ),
                  ).customCursorIsClickable,
                ),
              ],
            ),
          ),
        ),
      ],
    ).showCustomCursorOnHover;
  }
}
