import 'package:flutter/material.dart';
import 'package:made/View/leftPanel/web/Contact.dart';
import 'package:made/View/leftPanel/web/Social.dart';
import 'package:made/constants.dart';

class LeftPanelWeb extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        SizedBox(
          height: MediaQuery.of(context).size.height -
              ProjectConstants.HEADER_HEIGHT -
              ProjectConstants.FOOTER_HEIGHT,
          width: ProjectConstants.WEB_SOCIAL_WIDTH,
          child: Social(),
        ),
        Contact(),
      ],
    );
  }
}