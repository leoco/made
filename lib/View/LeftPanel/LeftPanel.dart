import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:made/View/LeftPanel/web/LeftPanelWeb.dart';

class LeftPanel extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ScreenTypeLayout.builder(
        mobile: (BuildContext context) => Container(),
        desktop: (BuildContext context) => LeftPanelWeb(),
      );
}
