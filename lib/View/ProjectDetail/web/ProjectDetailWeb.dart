import 'package:flutter/material.dart';
import 'package:made/Model/Project.dart';
import 'package:made/View/Asset/AssetBigImage.dart';
import 'package:made/View/Asset/AssetSmallImages.dart';
import 'package:made/View/Asset/AssetVideo.dart';
import 'package:made/View/Asset/AssetYoutube.dart';
import 'package:made/constants.dart';
import 'package:made/routes.dart';
import 'package:made/Tools/HoverExtensions.dart';

class ProjectDetailWeb extends StatelessWidget {
  ProjectDetailWeb({Key key, this.id}) : super(key: key);

  final int id;

  List<Widget> _mapAssetToWidget(List<Asset> assets) {
    return assets.map((asset) {
      switch (asset.assetType) {
        case AssetType.youtube:
          return AssetYoutube(link: asset.assetLocations[0]);
        case AssetType.bigImage:
          return AssetBigImage(
            location: asset.assetLocations[0],
          );
          break;
        case AssetType.smallImages:
          return AssetSmallImages(
            locations: asset.assetLocations,
          );
          break;
        case AssetType.video:
          return AssetVideo(
            location: asset.assetLocations[0],
            hasControl: false,
          );
          break;
        case AssetType.videoWithControl:
          return AssetVideo(
            location: asset.assetLocations[0],
            hasControl: true,
          );
          break;
        default:
          return Container();
      }
    }).toList();
  }

  void _onTap() {
    ProjectRoutes.navigatorKey.currentState
        .pushNamedAndRemoveUntil('projects', (route) => false);
  }

  @override
  Widget build(BuildContext context) {
    final Project project = ProjectConstants.projects[id];
    final int length = ProjectConstants.projects.length;
    final assetsList = _mapAssetToWidget(project.assets);
    final double totalWidth = (MediaQuery.of(context).size.width -
        2 * ProjectConstants.WEB_SOCIAL_WIDTH);

    return Padding(
      padding: const EdgeInsets.only(right: ProjectConstants.WEB_SOCIAL_WIDTH),
      child: ListView(
        physics: BouncingScrollPhysics(),
        children: <Widget>[
          Row(
            mainAxisSize: MainAxisSize.max,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              // First column with title and description
              Expanded(
                flex: 40,
                child: Container(
                  padding: EdgeInsets.only(top: 50, left: 10),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Padding(
                        padding: const EdgeInsets.only(bottom: 50.0),
                        child: InkWell(
                          highlightColor: Colors.transparent,
                          hoverColor: Colors.transparent,
                          onTap: _onTap,
                          child: Text(
                            'Retour aux projets',
                            style: TextStyle(
                                fontFamily: 'MonumentExtended',
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline),
                          ),
                        ).customCursorIsClickable,
                      ),
                      Wrap(
                        direction: Axis.horizontal,
                        children: <Widget>[
                          project.upperText != null
                              ? Hero(
                                  tag: project.upperText,
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Text(
                                      project.upperText,
                                      textAlign: TextAlign.start,
                                      style: TextStyle(
                                        fontFamily: 'MonumentExtended',
                                        fontSize: 30,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
                          project.bottomText != null
                              ? Hero(
                                  tag: project.bottomText,
                                  child: FittedBox(
                                    fit: BoxFit.contain,
                                    child: Text(
                                      " " + project.bottomText,
                                      style: TextStyle(
                                        fontFamily: 'MonumentExtended',
                                        fontSize: 30,
                                        fontWeight: FontWeight.w400,
                                      ),
                                    ),
                                  ),
                                )
                              : Container(),
                        ],
                      ),
                      FittedBox(
                        fit: BoxFit.contain,
                        child: Padding(
                          padding: EdgeInsets.only(top: 40),
                          child: Wrap(
                            direction: Axis.horizontal,
                            children: project.tags
                                .map(
                                  (tag) => Text(
                                    "$tag • ".toUpperCase(),
                                    style: TextStyle(
                                      color: Color(0xffa7a7a7),
                                      fontSize: 18,
                                      fontWeight: FontWeight.w800,
                                    ),
                                  ),
                                )
                                .toList(),
                          ),
                        ),
                      ),
                      Padding(
                        padding: EdgeInsets.only(top: 40),
                        child: Text(
                          project.bigDescription,
                          textAlign: TextAlign.justify,
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w400,
                            height: 2,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
              // Image
              Expanded(
                flex: 60,
                child: Container(
                  child: Hero(
                    tag: project.name,
                    child: Image.asset(
                      project.bgImage,
                      fit: BoxFit.contain,
                    ),
                  ),
                ),
              ),
            ],
          ),
          Align(
            alignment: Alignment.centerLeft,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              children: assetsList,
            ),
          ),
          Container(
            height: ProjectConstants.FOOTER_HEIGHT,
            width: totalWidth,
            child: Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                GestureDetector(
                  onTap: () => ProjectRoutes.navigatorKey.currentState
                      .pushNamed('projects/${(id - 1) % length}'),
                  child: Text(
                    'Projet précédent',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ).customCursorIsClickable,
                GestureDetector(
                  onTap: () => ProjectRoutes.navigatorKey.currentState
                      .pushNamed('projects/${(id + 1) % length}'),
                  child: Text(
                    'Projet suivant',
                    style: TextStyle(
                      fontSize: 20,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ).customCursorIsClickable,
              ],
            ),
          ),
        ],
      ),
    );
  }
}
