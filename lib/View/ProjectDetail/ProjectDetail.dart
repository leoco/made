import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:made/View/ProjectDetail/web/ProjectDetailWeb.dart';

class ProjectDetail extends StatelessWidget {
  ProjectDetail({Key key, this.id}) : super(key: key);

  final int id;

  @override
  Widget build(BuildContext context) => ScreenTypeLayout.builder(
    mobile: (BuildContext context) => Container(),
    desktop: (BuildContext context) => ProjectDetailWeb(id: id),
  );
}