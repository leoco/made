import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';

class ClickButton extends StatefulWidget {
  final Function _onTap;

  ClickButton({Key key, @required Function onTap})
      : _onTap = onTap,
        super(key: key);

  @override
  _ClickButtonState createState() => _ClickButtonState();
}

class _ClickButtonState extends State<ClickButton> {

  static const double BUTTON_SIZE = 50;
  bool isHovered = false;

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      onEnter: (_) => setState(() {
        isHovered = true;
      }),
      onExit: (_) => setState(() {
        isHovered = false;
      }),
      child: MaterialButton(
        elevation: 0,
        animationDuration: Duration.zero,
        color: isHovered ? Colors.black : Colors.white,
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(BUTTON_SIZE/2),
          side: BorderSide(
            color: Colors.black,
            width: 1.0,
          ),
        ),
        height: BUTTON_SIZE,
        onPressed: widget._onTap,
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 10.0),
          child: Text(
            "En savoir plus",
            style: TextStyle(
              color: isHovered ? Colors.white : Colors.black,
              fontSize: 18,
            ),
          ),
        ),
      ),
    );
  }
}
