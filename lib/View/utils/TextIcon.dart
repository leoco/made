import 'dart:html' as html;
import 'package:flutter/material.dart';

class TextIcon extends StatelessWidget {
  const TextIcon({Key key, this.text, this.icon, this.url}) : super(key: key);

  final String text;
  final IconData icon;
  final String url;

  void _onTap() async {
    html.window.open(url, this.text);
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: _onTap,
      child: Row(
        children: <Widget>[
          Icon(
            this.icon,
            color: Colors.grey,
            size: 12,
          ),
          Padding(
            padding: EdgeInsets.only(left: 3.0),
            child: Semantics(
              link: true,
              child: Text(
                this.text,
                style: TextStyle(
                  fontWeight: FontWeight.w800,
                  fontSize: 12,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }
}
