import 'package:flutter/material.dart';

import 'package:made/tools/HoverExtensions.dart';

class AssetSmallImages extends StatelessWidget {
  AssetSmallImages({Key key, this.locations}) : super(key: key);

  final List<String> locations;

  @override
  Widget build(BuildContext context) {
    return Wrap(
      alignment: WrapAlignment.center,
      spacing: 80,
      runSpacing: 80,
      children: locations
          .map(
            (imageLocation) => SizedBox(
              width: 400,
              child: AnimatedImage(
                location: imageLocation,
              ),
            ),
          )
          .toList(),
    );
  }
}

class AnimatedImage extends StatefulWidget {
  AnimatedImage({Key key, this.location}) : super(key: key);

  final String location;

  @override
  _AnimatedImageState createState() => _AnimatedImageState();
}

class _AnimatedImageState extends State<AnimatedImage>
    with SingleTickerProviderStateMixin {
  double _elevation = 2.0;

  AnimationController _controller;
  Animation<double> _scaleAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this,
      duration: Duration(milliseconds: 100),
    );
    _scaleAnimation = Tween<double>(
      begin: 1.0,
      end: 1.05,
    ).animate(CurvedAnimation(parent: _controller, curve: Curves.easeInOut));
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  void _onEnter(_) {
    setState(() {
      _elevation = 10.0;
    });
    _controller.forward();
  }

  void _onExit(_) {
    setState(() {
      _elevation = 2.0;
    });
    _controller.reverse();
  }

  @override
  Widget build(BuildContext context) {
    return ScaleTransition(
      scale: _scaleAnimation,
      child: AnimatedPhysicalModel(
        color: Colors.black,
        shadowColor: Colors.black,
        shape: BoxShape.rectangle,
        duration: Duration(
          milliseconds: 100,
        ),
        elevation: _elevation,
        curve: Curves.easeInOut,
        child: MouseRegion(
          onEnter: _onEnter,
          onExit: _onExit,
          child: Image.asset(widget.location),
        ),
      ).customCursorIsClickable,
    );
  }
}
