import 'package:flutter/material.dart';
import 'package:youtube_player_iframe/youtube_player_iframe.dart';

class AssetYoutube extends StatelessWidget {
  final double _aspectRatio;
  final YoutubePlayerController _controller;

  AssetYoutube({Key key, @required String link, double aspectRatio})
      : _aspectRatio = aspectRatio ?? 16 / 9,
        _controller = YoutubePlayerController(
          initialVideoId: link,
          params: YoutubePlayerParams(
            showControls: false,
            autoPlay: true,
            showFullscreenButton: false,
          ),
        ),
        super(key: key);

  @override
  Widget build(BuildContext context) {
    return IgnorePointer(
      child: YoutubePlayerIFrame(
        gestureRecognizers: {},
        controller: _controller,
        aspectRatio: _aspectRatio,
      ),
    );
  }
}
