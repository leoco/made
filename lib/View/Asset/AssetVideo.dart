import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:video_player/video_player.dart';

import 'package:made/constants.dart';
import 'package:made/tools/HoverExtensions.dart';

class AssetVideo extends StatefulWidget {
  AssetVideo({Key key, this.location, this.hasControl}) : super(key: key);

  final String location;
  final bool hasControl;

  @override
  _AssetVideoState createState() => _AssetVideoState();
}

class _AssetVideoState extends State<AssetVideo> {
  VideoPlayerController _controller;
  Future<void> _initializeVideoPlayerFuture;

  @override
  void initState() {
    _controller = VideoPlayerController.asset(widget.location);

    _initializeVideoPlayerFuture = _controller.initialize();

    super.initState();
  }

  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
      future: _initializeVideoPlayerFuture,
      builder: (context, snapshot) {
        if (snapshot.connectionState == ConnectionState.done) {
          _controller.setLooping(true);
          if (!widget.hasControl) _controller.play();
          return SizedBox(
            width: MediaQuery.of(context).size.width -
                ProjectConstants.WEB_SOCIAL_WIDTH * 2,
            child: AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: <Widget>[
                  VideoPlayer(_controller),
                  if (widget.hasControl)
                    _PlayPauseOverlay(controller: _controller),
                  if (widget.hasControl)
                    VideoProgressIndicator(_controller, allowScrubbing: true),
                ],
              ),
            ),
          );
        } else {
          return Center(child: CircularProgressIndicator());
        }
      },
    ).customCursorIsClickable;
  }
}

class _PlayPauseOverlay extends StatefulWidget {
  const _PlayPauseOverlay({Key key, this.controller}) : super(key: key);

  final VideoPlayerController controller;

  @override
  __PlayPauseOverlayState createState() => __PlayPauseOverlayState();
}

class __PlayPauseOverlayState extends State<_PlayPauseOverlay> {
  bool isHover = false;
  @override
  void initState() {
    super.initState();
    widget.controller.addListener(() {
      setState(() {});
    });
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        MouseRegion(
          onEnter: (event) => setState(() {
            isHover = true;
          }),
          onExit: (event) => setState(() {
            isHover = false;
          }),
          child: AnimatedSwitcher(
              duration: Duration(milliseconds: 1000),
              reverseDuration: Duration(milliseconds: 1000),
              child: isHover
                  ? Container(
                      color: Colors.black38,
                      child: Center(
                        child: Icon(
                          widget.controller.value.isPlaying
                              ? FontAwesomeIcons.pause
                              : FontAwesomeIcons.play,
                          color: Colors.white,
                          size: 100.0,
                        ),
                      ),
                    )
                  : Container()),
        ),
        GestureDetector(
          onTap: () {
            widget.controller.value.isPlaying
                ? widget.controller.pause()
                : widget.controller.play();
          },
        ),
      ],
    );
  }
}
