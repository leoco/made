import 'package:flutter/material.dart';

import 'package:made/constants.dart';

class AssetBigImage extends StatelessWidget {
  AssetBigImage({Key key, this.location}) : super(key: key);

  final String location;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width -
          ProjectConstants.WEB_SOCIAL_WIDTH * 2,
      child: Image.asset(
        location,
        fit: BoxFit.fitWidth,
      ),
    );
  }
}
