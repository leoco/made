import 'package:flutter/material.dart';
import 'package:made/View/leftPanel/web/ContactForm.dart';

import 'package:made/routes.dart';
import 'package:made/tools/HoverExtensions.dart';
import 'package:made/tools/ParallaxExtensions.dart';

class HomePageWeb extends StatelessWidget {
  static const _yellowFactorHeight = 0.883 * 1.39;
  static const _yellowFactorWidth = 0.556 * 1.39;
  static const _redFactorHeight = 0.544 * 1.4;
  static const _redFactorWidth = 0.243 * 1.4;
  static const _linesFactorHeight = 0.424 * 1.38;
  static const _linesFactorWidth = 0.374 * 1.38;
  static const _columnFactorWidth = 0.22;

  @override
  Widget build(BuildContext context) {
    final screenSize = MediaQuery.of(context).size;
    return SizedBox(
      width: screenSize.width,
      height: screenSize.height,
      child: Stack(
        fit: StackFit.expand,
        overflow: Overflow.visible,
        children: <Widget>[
          Positioned(
            top: -screenSize.height / 3,
            left: -screenSize.width / 2.7,
            height: screenSize.height * _yellowFactorHeight,
            width: screenSize.width * _yellowFactorWidth,
            child: ExcludeSemantics(
              child: Image.asset(
                "assets/forme-jaune.png",
                fit: BoxFit.contain,
              ).withMouseParallax(factor: 0.01, invertX: true, invertY: false),
            ),
          ),
          ExcludeSemantics(
            child: Align(
              alignment: Alignment.centerRight,
              child: SizedBox(
                height: screenSize.height * _redFactorHeight,
                width: screenSize.width * _redFactorWidth,
                child: Transform.translate(
                  offset: Offset(
                    screenSize.width / 4.5,
                    -screenSize.height / 7,
                  ),
                  child: Image.asset(
                    "assets/forme-rouge.png",
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ).withMouseParallax(factor: 0.01, invertX: false, invertY: false),
          ),
          ExcludeSemantics(
            child: Align(
              alignment: Alignment.bottomLeft,
              child: SizedBox(
                height: screenSize.height * _linesFactorHeight,
                width: screenSize.width * _linesFactorWidth,
                child: Transform.translate(
                  offset: Offset(
                    0,
                    screenSize.height / 10,
                  ),
                  child: Image.asset(
                    "assets/petits-traits.png",
                    fit: BoxFit.contain,
                  ),
                ),
              ),
            ).withMouseParallax(factor: 0.01, invertX: false, invertY: true),
          ),
          ExcludeSemantics(
            child: Align(
              alignment: Alignment.center,
              child: SizedBox(
                width: MediaQuery.of(context).size.width / 2,
                child: Image.asset("assets/homeface.png"),
              ),
            ).withMouseParallax(factor: 0.03, invertX: true, invertY: true),
          ),
          Positioned(
            top: screenSize.height / 1.9,
            left: screenSize.width / 1.7,
            width: screenSize.width * _columnFactorWidth,
            child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    "Hello, moi c’est Madisson,",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
                FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    "la fondatrice de made !",
                    style: TextStyle(
                      fontSize: 28,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20.0),
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Text(
                      "Élaborons ensemble les supports",
                      style: TextStyle(
                        fontSize: 22,
                        fontWeight: FontWeight.w400,
                      ),
                    ),
                  ),
                ),
                FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    "qui feront décoller votre projet.",
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40.0),
                  child: FittedBox(
                    fit: BoxFit.contain,
                    child: Row(
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        GestureDetector(
                          onTap: () => showDialog(
                            context: context,
                            builder: (context) => ContactForm(),
                          ),
                          child: Text(
                            "collaborons",
                            style: TextStyle(
                              fontFamily: 'MonumentExtended',
                              fontSize: 14,
                              fontWeight: FontWeight.w400,
                              decoration: TextDecoration.underline,
                            ),
                          ).customCursorIsClickable,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: 40.0),
                          child: GestureDetector(
                            onTap: () => ProjectRoutes.navigatorKey.currentState
                                .pushNamedAndRemoveUntil(
                                    'projects', (_) => false),
                            child: Text(
                              "voir mes projets",
                              style: TextStyle(
                                fontFamily: 'MonumentExtended',
                                fontSize: 14,
                                fontWeight: FontWeight.w400,
                                decoration: TextDecoration.underline,
                              ),
                            ),
                          ).customCursorIsClickable,
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}
