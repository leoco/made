import 'package:flutter/material.dart';
import 'package:responsive_builder/responsive_builder.dart';

import 'package:made/View/homepage/web/HomepageWeb.dart';
import 'package:made/View/homepage/mobile/HomePageMobile.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) => ScreenTypeLayout.builder(
    mobile: (BuildContext context) => HomePageMobile(),
    desktop: (BuildContext context) => HomePageWeb(),
  );
}
