import 'package:flutter/material.dart';
import 'package:made/constants.dart';

class HomePageMobile extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Stack(
      fit: StackFit.expand,
      overflow: Overflow.visible,
      children: [
        Padding(
          padding: const EdgeInsets.fromLTRB(
              30, 0, 30, 60),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.end,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Image.asset(
                "assets/homeface.png",
                fit: BoxFit.contain,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 60),
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    "Hello, moi c'est Madisson,\nla fondatrice de made !",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 24,
                      fontWeight: FontWeight.w800,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: FittedBox(
                  fit: BoxFit.contain,
                  child: Text(
                    "Élaborons ensemble les supports\nqui feront décoller votre projet.",
                    textAlign: TextAlign.left,
                    style: TextStyle(
                      fontSize: 22,
                      fontWeight: FontWeight.w400,
                    ),
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 20),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: [
                    Text(
                      "collaborons",
                      style: TextStyle(
                          fontFamily: "MonumentExtended",
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.underline),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(left: 20.0),
                      child: Text(
                        "voir mes projets",
                        style: TextStyle(
                          fontFamily: "MonumentExtended",
                          fontSize: 13,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.underline,
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ],
    );
  }
}
