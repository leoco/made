import 'dart:html' as html;
import 'package:flutter/material.dart';

extension ParallaxExtensions on Widget {
  static Stream<html.MouseEvent> _mouseEvents =
      html.window.document.onMouseMove;

  Widget withMouseParallax({
    double factor = 0.02,
    bool invertX = false,
    bool invertY = false,
  }) {
    double factorX = invertX ? -factor : factor;
    double factorY = invertY ? -factor : factor;
    return StreamBuilder<html.MouseEvent>(
      stream: _mouseEvents,
      builder: (BuildContext context, AsyncSnapshot<html.MouseEvent> snapshot) {
        if (snapshot.connectionState != ConnectionState.active)
          return this;
        else {
          final size = MediaQuery.of(context).size;
          Offset offset = Offset(
            (snapshot.data.offset.x - size.width / 2) * factorX,
            (snapshot.data.offset.y - size.height / 2) * factorY,
          );
          return RepaintBoundary(
            child: Transform.translate(
              offset: offset,
              child: this,
            ),
          );
        }
      },
    );
  }
}
