import 'dart:async';
import 'dart:html' as html;
import 'package:flutter/foundation.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

extension HoverExtensions on DiagnosticableTree {
  // Get a reference to the body of the view
  static final appContainer =
      html.window.document.getElementById('app-container');

  static StreamController<bool> hoverStream =
      StreamController.broadcast(sync: true);

  Widget get customCursorIsClickable {
    return MouseRegion(
      child: this,
      // When the mouse enters the widget set the cursor to pointer
      onEnter: (event) => hoverStream.add(true),
      // When it exits set it back to default
      onExit: (event) => hoverStream.add(false),
    );
  }

  Widget get showCustomCursorOnHover {
    return Stack(
      fit: StackFit.expand,
      children: <Widget>[
        RepaintBoundary(child: this),
        ExcludeSemantics(
          excluding: true,
          child: RepaintBoundary(
            child: IgnorePointer(
              child: CustomCursor(),
            ),
          ),
        ),
      ],
    );
  }
}

class CustomCursor extends StatefulWidget {
  @override
  _CustomCursorState createState() => _CustomCursorState();
}

class _CustomCursorState extends State<CustomCursor>
    with SingleTickerProviderStateMixin {
  Offset mouseRegion = Offset.zero;

  AnimationController controller;
  Animation<double> scaleAnimation;

  @override
  void initState() {
    super.initState();
    HoverExtensions.appContainer.style.cursor = 'none';
    controller = AnimationController(
      duration: Duration(milliseconds: 100),
      vsync: this,
    );
    scaleAnimation = Tween<double>(
      begin: 1.0,
      end: 3.0,
    ).animate(
      CurvedAnimation(
        parent: controller,
        curve: Curves.linear,
      ),
    );
    HoverExtensions.hoverStream.stream.listen((event) {
      if (event) {
        controller.forward();
      } else {
        controller.reverse();
      }
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void _onHover(Offset position) {
    setState(() {
      mouseRegion = position;
    });
  }

  @override
  Widget build(BuildContext context) {
    return MouseRegion(
      opaque: false,
      onHover: (event) => _onHover(event.position),
      child: AnimatedBuilder(
        animation: scaleAnimation,
        builder: (context, child) {
          return CustomPaint(
            painter: CustomCursorPainter(
              mouseOffset: mouseRegion,
              scaleValue: scaleAnimation.value,
            ),
          );
        },
      ),
    );
  }
}

class CustomCursorPainter extends CustomPainter {
  CustomCursorPainter({this.mouseOffset, this.scaleValue});

  final Offset mouseOffset;
  final double scaleValue;

  @override
  void paint(Canvas canvas, Size size) {
    Paint paint = Paint()..color = Colors.black26;
    canvas.drawCircle(mouseOffset, 10 * scaleValue, paint);
  }

  @override
  bool shouldRepaint(CustomCursorPainter old) {
    return (old.mouseOffset != mouseOffset || old.scaleValue != scaleValue);
  }
}
