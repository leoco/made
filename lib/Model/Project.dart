import 'dart:ui';

class Project {
  const Project({
    this.name,
    this.upperText,
    this.bottomText,
    this.description,
    this.bigDescription,
    this.bgColor,
    this.bgImage,
    this.tags,
    this.assets,
  });

  final String name;
  final String upperText;
  final String bottomText;
  final String description;
  final Color bgColor;
  final String bgImage;
  final String bigDescription;
  final List<String> tags;
  final List<Asset> assets;
}

class Asset {
  const Asset(this.assetType, this.assetLocations);

  final AssetType assetType;
  final List<String> assetLocations;
}

enum AssetType {
  bigImage,
  smallImages,
  video,
  videoWithControl,
  youtube,
}
