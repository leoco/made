class Illustration {

  final List<String> tags;
  final String asset;
  final String name;
  final String description;

  const Illustration({this.tags, this.asset, this.name, this.description});
}