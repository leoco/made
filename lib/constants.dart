import 'dart:math';

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'package:made/Model/Project.dart';
import 'package:made/Model/Illustration.dart';
import 'package:made/View/utils/TextIcon.dart';

class ProjectConstants {
  ProjectConstants._();

  ////////////////////////////////////////////////////
  ///Changer ici les differentes illustrations
  ////////////////////////////////////////////////////
  static const List<Illustration> illustrations = [
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/cat.webp",
      name: "Cat",
      description: "Chat dans rue Parisienne, réalisé avec Concepts",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/mushrooms.webp",
      name: "Champignons",
      description: "Champignons - Illustrator",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/theo.webp",
      name: "Portrait d'un homme",
      description: "Commande d'un client privé, réalisée sous Adobe Sketch",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/cat.webp",
      name: "Cat",
      description: "Acrylique sur toile 40cmx30cm",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/plantesbd.gif",
      name: "Plantes BD",
      description: "Acrylique sur toile 40cmx30cm",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/bioma.gif",
      name: "Bioma",
      description: "Illustration pour le livret Platelet Safety ~ bioMérieux",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/POUSSIN.png",
      name: "Poussin",
      description: "Poussin qui fait du tenis ~ Illustrator",
    ),
    Illustration(
      tags: ["Photo"],
      asset: "assets/illustrations/photo/DIOR.jpg",
      name: "Dior",
      description: "Reprise des codes photos de l'affiche de Dior",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/JEU_DE_LOI.gif",
      name: "Jeu de loi",
      description: "Plateau de jeu de l'Oie sur les lois ~ échelle humaine",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/petits-monstres.gif",
      name: "Petits monstres",
      description: "Petits monstres ~ Aquarelle",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/MOON.jpg",
      name: "Moon",
      description: "Illustration sous Photoshop",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/heart_failure.gif",
      name: "Heart Failure",
      description: "Illustration pour le livret Heart Failure ~ bioMérieux",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/leo.jpg",
      name: "Leo",
      description: "Portait sous illustrator",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/bricolo.jpg",
      name: "Bricolo",
      description:
          "Personnage expressif sous illustrator ~ un tournevis bricoleur ",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/saxo.jpg",
      name: "Saxo Print",
      description: "Illustrations et affiche ~ référence au mille bornes",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/maman.jpg",
      name: "Maman",
      description: "Portait sous illustrator",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/NETFLIX.jpg",
      name: "Netflix",
      description: "Louanges à Netflix ~ Illustrator",
    ),
    Illustration(
      tags: ["Digital"],
      asset: "assets/illustrations/digital/papiertoilette.gif",
      name: "Papier toilette",
      description:
          "Illustrations pour un papier toilette écologique ~ Illustrator",
    ),
    Illustration(
      tags: ["Photo"],
      asset: "assets/illustrations/photo/guerlain.jpg",
      name: "Guerlain",
      description: "Reprise des codes photos de l'affiche de Guerlain",
    ),
    Illustration(
      tags: ["Photo"],
      asset: "assets/illustrations/photo/still_life.gif",
      name: "Still Life",
      description:
          "Univers minimaliste et vintage pour une boite à oeufs ~ Hema",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/dunebd.jpg",
      name: "Dune BD",
      description: "Acrylique sur toile 30x30cm",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/ecoliere.jpg",
      name: "Ecoliere",
      description: "Aquarelle stylisé",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/leo.jpg",
      name: "Leo",
      description: "Croquis ~ Stylo bic",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/perspective.jpg",
      name: "Perspective",
      description: "Croquis ~ Mine de plomb",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/perspective_bellecour.gif",
      name: "Perspective Bellecour",
      description: "Croquis ~ Encres",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/SINGE.gif",
      name: "Singe",
      description: "Croquis de La Planète des Singes ~ Stylo bic",
    ),
    Illustration(
      tags: ["tradi"],
      asset: "assets/illustrations/tradi/val.jpg",
      name: "Valentine",
      description: "Croquis longue pose ~ Crayon à papier",
    ),
  ];

  ////////////////////////////////////////////////////
  ///Changer ici les differents projets
  ////////////////////////////////////////////////////
  static const List<Project> projects = [
    Project(
        name: '01  fulll',
        upperText: 'fulll',
        description: "Création de Design System et d'Interfaces",
        bgColor: Color(0xFF6CB8F6),
        bgImage: 'assets/bg-img/home_fulll.webp',
        tags: [
          "ux/ui design",
          "design system"
        ],
        bigDescription:
            '''fulll a pour mission d'aider les experts-comptables et les chefs d'entreprises à piloter leur activité et à accompagner leurs clients. Suite à la fusion de ses éditeurs fulll souhaite lisser ses interfaces UX/UI. Pour cela nous avons crée un design system documenté et détaillé pour l’usage et les specs de chaque composant. En parallèle de cette mission je conçois et prototype des interfaces en haute fidélité, je fais tester le parcours utilisateur et itère si besoin.

fulll c'est :
- Plus de 230 composants accessibles documentés
- 600 cabinets d'expertise-comptable accompagnés quotidiennement
- 150 000 entreprises connectées.''',
        assets: [
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/1.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/2.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/3.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/4.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/5.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/6.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/7.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/1_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/2_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/3_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/4_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/5_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/6_CRM.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/6_CRM.webp"],
          ),
          Asset(
            AssetType.videoWithControl,
            ["assets/projects-img/fulll/2-video.webm"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/Fulll-1.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/Fulll-2.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/Fulll-3.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/Fulll-4.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/fulll/Fulll-5.webp"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/fulll/3-favoris.webm"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/fulll/4-formulaire.webm"],
          ),
        ]),
    Project(
        name: '02  Ridevizor',
        upperText: 'Ridevizor',
        description: "Projet entrepreunarial, création d'une application BtoB",
        bgColor: Color(0xFFDDAFFF),
        bgImage: 'assets/bg-img/ridevizor.png',
        tags: ["ux design", "ui design", "persona"],
        bigDescription:
            "Création d'une application BtoB afin de résoudre les problèmes auxquels les chauffeurs VTC font face. Ridevizor sélectionne les meilleures courses disponibles autour du chauffeur depuis ses différents comptes VTC, ainsi il gagne plus d'argent tout en centralisant ses données.",
        assets: [
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/PROBLEMATIQUE.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/BUDGET.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/PERSONA.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/ECHEANCE.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/TECHNIQUES.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/MARCHE_POTENTIEL.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/EXPERIENCE_MAP.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/FONCTIONS-PRINCIPALES.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/CONCURRENCE.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/STRATEGIE_COMMUNICATION.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/LOGO.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/STYLE_GUIDE.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/BOUTON.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/USER_FLOW.webp"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/ridevizor/INSCRIPTION.webm"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/ABONNEMENT.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/PAIEMENT_TUTO.webp"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/ridevizor/COMPTE.webm"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/CONTEXTE.webp"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/ridevizor/FILTRES.webm"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/ridevizor/COURSE.webm"],
          ),
          Asset(
            AssetType.video,
            ["assets/projects-img/ridevizor/HISTORIQUE.webm"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/ridevizor/MORE_SCREENS.webp"],
          ),
          Asset(
            AssetType.videoWithControl,
            ["assets/projects-img/ridevizor/ride-add.webm"],
          ),
          Asset(
            AssetType.videoWithControl,
            ["assets/projects-img/ridevizor/website-ridevizor.webm"],
          ),
        ]),
    Project(
      name: '03  Emoveo',
      upperText: 'Emoveo',
      description: "Création d’une start-up et de son concept",
      bgColor: Color(0xffffd5d5),
      bgImage: 'assets/bg-img/emoveo.webp',
      tags: ["business plan", "business model", "branding"],
      bigDescription:
          "Création d’une start-up et de son concept afin de venir en aide sur un problème de la société. Emoveo est un service de mise en relation de particuliers en zone rurale pour de la livraison de courses alimentaires provenant de petits commerces de proximité.",
      assets: [
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO_1.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO_2.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO_3.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO – UX – 1.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO – UX – 2.png"],
        ),
        Asset(
          AssetType.videoWithControl,
          ["assets/projects-img/emoveo/EMOVEO_4.webm"],
        ),
        Asset(
          AssetType.video,
          ["assets/projects-img/emoveo/EMOVEO_5.webm"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO_6.webp"],
        ),
        Asset(
          AssetType.video,
          ["assets/projects-img/emoveo/EMOVEO_7.webm"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/CHARTE.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/CHARTE_DETAIL.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/TYPO.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/COULEURS.gif"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/AFFICHES.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/CHARTE_DETAIL – 1.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/CHARTE_DETAIL – 2.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO – UX – 3.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO – UX – 5.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/emoveo/EMOVEO_9.webp"],
        ),
      ],
    ),
    Project(
        name: '04  Footmercato',
        upperText: 'Foot',
        bottomText: 'Mercato',
        description:
            'Repenser l’UX/UI du site existant pour les pages des clubs de football',
        bgColor: Color(0xFF111D83),
        bgImage: 'assets/bg-img/footmercato.webp',
        tags: ["ux/ui design"],
        bigDescription:
            "Repenser l’UX/UI du site existant pour les pages des clubs de football.",
        assets: [
          Asset(
            AssetType.video,
            ["assets/projects-img/footmercato/FOOTMERCATO_1.webm"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/footmercato/FOOTMERCATO_2.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/footmercato/FOOTMERCATO_3.webp"],
          ),
          Asset(
            AssetType.bigImage,
            ["assets/projects-img/footmercato/FOOTMERCATO_4.webp"],
          ),
        ]),
    Project(
      name: '05  Corner Café',
      upperText: 'Corner',
      bottomText: "Café",
      description: "Aménagement d'un espace café au sein d'une école",
      bgColor: Color(0xff908134),
      bgImage: 'assets/bg-img/corner-cafe.webp',
      tags: ["sprint design", "map experience", "test utilisateur"],
      bigDescription:
          "Création d’un espace café et d’une solution digitale dans une école. Réaliser dans le cadre d’un sprint design avec des méthodes UX : persona, wireframes, storyboarding, animation en LEGO® SERIOUS PLAY® et prototypage.",
      assets: [
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/PERSONA.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/OBSERVATIONS.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/CUSTOMER_JOURNEY_MAP.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/SOLUTIONS.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/PHOTO_1.png"],
        ),
        Asset(
          AssetType.video,
          ["assets/projects-img/corner-cafe/UX_UI_CORNER.mp4"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/PHOTO_2.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/CORNER_AMENAGEMENT.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/CORNER_VUE.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/RETOURS_UTILISATEURS.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/corner-cafe/ACTIONS_FUTURES.png"],
        ),
      ],
    ),
    Project(
      name: '06  Fête de la musique',
      upperText: 'Fête de',
      bottomText: "la musique",
      description: "Campagne d'affichage nationale",
      bgColor: Color(0xffd2d2d2),
      bgImage: 'assets/bg-img/fdlm.png',
      tags: ["identité", "affichage"],
      bigDescription:
          "Campagne d’affichage nationale pour La Fête de la Musique. Cette fête est un lieu de découverte, de et de voyage. Elle apparaît comme un lieu mystique évoquant un 7e continent. Les cartes géographiques m'ont inspirées.",
      assets: [
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_1.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_2.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_3.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_4.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_5.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_6.jpg"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_7.jpg"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_8.jpg"],
        ),
        Asset(
          AssetType.video,
          ["assets/projects-img/fdlm/FACEBOOK.mp4"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_9.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/fdlm/FDLM_10.png"],
        ),
      ],
    ),
    Project(
      name: '07  Volubilis',
      upperText: 'Volubilis',
      description: "Création d'une marque de vin Malbec",
      bgColor: Color(0xfffeb429),
      bgImage: 'assets/bg-img/volubilis.webp',
      tags: ["identité", "étiquetage", "rédaction"],
      bigDescription:
          "Accompagnement du viticulteur pour la création de sa marque de vin : son nom, son histoire et son design. Son vin de Malbec est une gamme prestigieuse aux saveurs corsées. Volubilis 2008 tire ses origines d’un terroir gravelo-argileux de Reconquista (Argentine) qui lui permet d’exprimer la puissance et la délicatesse du cépage de Malbec. Son nom fait référence au Volubilis, fleur de liseron bleue, fragile en apparence et pourtant... Ne laissez plus le mystère et dégustez ce vin aux saveurs délectables.",
      assets: [
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/volubilis/VOLUBILIS_1.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/volubilis/VOLUBILIS_2.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/volubilis/VOLUBILIS_3.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/volubilis/VOLUBILIS_4.webp"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/volubilis/VOLUBILIS_5.webp"],
        ),
      ],
    ),
    Project(
      name: '08  Daily UI',
      upperText: 'Daily UI',
      description: "Challenge de conception",
      bgColor: Color(0xff84E774),
      bgImage: 'assets/bg-img/dailyui.png',
      tags: ["Application", "Site web", "Interface"],
      bigDescription:
          "Daily UI est une série de défis de conception quotidiens qui développe l'inspiration et la créativité.",
      assets: [
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/dailyui/DAILYUI6.png"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/dailyui/DAILYUI7.png"],
        ),
        Asset(
          AssetType.videoWithControl,
          ["assets/projects-img/dailyui/memoire.mp4"],
        ),
        Asset(
          AssetType.videoWithControl,
          ["assets/projects-img/dailyui/lucioles.mp4"],
        ),
        Asset(
          AssetType.videoWithControl,
          ["assets/projects-img/dailyui/pillpack.mp4"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/dailyui/pillpack.gif"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/dailyui/leboncoin.gif"],
        ),
        Asset(
          AssetType.bigImage,
          ["assets/projects-img/dailyui/uxfit.gif"],
        ),
      ],
    ),
    // Project(
    //   name: '07  Lecomte de Brégeot',
    //   upperText: 'Lecomte',
    //   bottomText: "de Brégeot",
    //   description: "Site de Discographie et présentation de l’artiste",
    //   bgColor: Color(0xFF49276e),
    //   bgImage: 'assets/bg-img/lcdb.webp',
    //   tags: ["expérience unique", "ux/ui", "wireframe"],
    //   bigDescription:
    //       "Le site web comprend une biographie et une discographie de l’artiste Lecomte de Brégeot. Il est réalisé avec une approche expérience unique afin de le différencier des autres artistes électro : la découverte de ses musiques devient une expérience ludique et singulière.",
    //   assets: [
    //     Asset(
    //       AssetType.smallImages,
    //       [
    //         "assets/projects-img/lcdb/lcdb-1.jpg",
    //         "assets/projects-img/lcdb/lcdb-2.jpg",
    //         "assets/projects-img/lcdb/lcdb-3.jpg",
    //         "assets/projects-img/lcdb/lcdb-4.jpg",
    //         "assets/projects-img/lcdb/lcdb-BIO.jpg",
    //       ],
    //     ),
    //   ],
    // ),
  ];

  ////////////////////////////////////////////////////
  ///Changer ici les liens dans la colonne de gauche
  ////////////////////////////////////////////////////
  static const List<TextIcon> socialLink = [
    TextIcon(
      icon: FontAwesomeIcons.dribbble,
      text: 'DRIBBBLE',
      url: 'https://dribbble.com/MadissonRolland',
    ),
    TextIcon(
      icon: FontAwesomeIcons.behance,
      text: 'BEHANCE',
      url: 'https://www.behance.net/madissonrolland_made/',
    ),
    TextIcon(
      icon: FontAwesomeIcons.instagram,
      text: 'INSTAGRAM',
      url: 'https://www.instagram.com/madisson_rolland/',
    ),
    TextIcon(
      icon: FontAwesomeIcons.linkedinIn,
      text: 'LINKEDIN',
      url: 'https://www.linkedin.com/in/madisson-rolland-a02229129/',
    ),
  ];

  ////////////////////////////////////////////////////
  ///Changer ici les références dans la page a propos
  ////////////////////////////////////////////////////
  static const List<String> references = [
    "renault trucks",
    "paris 2024",
    "biomérieux",
    "danone",
    "mérieux nutrisciences",
    "sia partners",
    "mci",
    "forumdimo",
    "ridevizor",
  ];

  ////////////////////////////////////////////////////
  ///Changer ici les constantes générales du site
  ////////////////////////////////////////////////////
  static const TRANSITION_BETWEEN_SCREENS_DURATION = 1000; // en millisecondes

  ////////////////////////////////////////////////////
  ///Changer ici les constantes pour le site sur ordi
  ////////////////////////////////////////////////////

  /// Design
  static const HEADER_HEIGHT = 100.0;
  static const FOOTER_HEIGHT = 100.0;
  static const WEB_SOCIAL_WIDTH = 120.0;

  /// General
  static const PROJECT_DURATION = 4; // en secondes
  static const PROJECT_TRANSITION_DURATION = 600; // en millisecondes

  ////////////////////////////////////////////////////
  ///Changer ici les constantes pour le site sur mobile
  ////////////////////////////////////////////////////

  // Get the string size of the longest name
  static int get maxProjectNameSize {
    Project maxProject = projects.reduce((value, element) {
      int maxValue =
          max<int>(value.upperText?.length ?? 0, value.bottomText?.length ?? 0);
      int maxElement = max<int>(
          element.upperText?.length ?? 0, element.bottomText?.length ?? 0);
      if (maxValue > maxElement)
        return value;
      else
        return element;
    });
    return max<int>(
        maxProject.upperText?.length, maxProject.bottomText?.length);
  }

  static int get maxProjectFooterSize {
    Project maxProject = projects.reduce((value, element) {
      int maxValue = value.name?.length ?? 0;
      int maxElement = element.name?.length ?? 0;
      if (maxValue > maxElement)
        return value;
      else
        return element;
    });
    return max<int>(
        maxProject.upperText?.length, maxProject.bottomText?.length);
  }

  static int get maxProjectName {
    Project maxProject = projects.reduce((value, element) {
      int maxValue =
          (value.upperText?.length ?? 0) + (value.bottomText?.length ?? 0);
      int maxElement =
          (element.upperText?.length ?? 0) + (element.bottomText?.length ?? 0);
      if (maxValue > maxElement)
        return value;
      else
        return element;
    });
    return (maxProject.upperText?.length ?? 0) +
        (maxProject.bottomText?.length ?? 0);
  }

  static int get maxProjectTags {
    Project maxProject = projects.reduce((value, element) {
      int maxValue = value.tags.reduce((a, b) => a + b).length;
      int maxElement = element.tags.reduce((a, b) => a + b).length;
      if (maxValue > maxElement)
        return value;
      else
        return element;
    });
    return maxProject.tags.reduce((a, b) => a + b).length;
  }

  static Size screenSize;
}
