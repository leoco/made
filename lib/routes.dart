import 'package:fluro/fluro.dart';
import 'package:flutter/material.dart' hide Router;
import 'package:made/View/digital/Digital.dart';
import 'package:made/View/homepage/HomePage.dart';

import 'View/MainContainer.dart';
import 'package:made/View/about/About.dart';
import 'package:made/View/ProjectList/ProjectListContainer.dart';
import 'package:made/View/ProjectDetail/ProjectDetail.dart';

class ProjectRoutes {
  ProjectRoutes._();

  static FluroRouter appRouter = FluroRouter();
  static FluroRouter embeddedRouter = FluroRouter();

  static final navigatorKey = GlobalKey<NavigatorState>();

  /// Initialize routes in the project
  static void initializeRoutes() {
    appRouter
      ..define(
        '/',
        handler: Handler(handlerFunc: (context, parameters) {
          return Scaffold(
            backgroundColor: Color(0xFFFFFFFF),
            body: MainContainer(),
          );
        }),
      )
      ..define(
        '/about',
        handler: Handler(
          handlerFunc: (context, parameters) => About(),
        ),
      )
      ..define(
        '/digital',
        handler: Handler(
          handlerFunc: (context, parameters) => Digital(),
        )
      )
      ..define(
        '/:initialRoute',
        handler: Handler(handlerFunc: (context, parameters) {
          return Scaffold(
            backgroundColor: Color(0xFFFFFFFF),
            body: MainContainer(initialRoute: "projects"),
          );
        }),
      );
    embeddedRouter
      ..define(
        'home',
        handler: Handler(
          handlerFunc: (context, parameters) => HomePage(),
        ),
      )
      ..define(
        'projects',
        handler: Handler(
          handlerFunc: (context, parameters) => ProjectListContainer(),
        ),
      )
      ..define(
        'projects/:id',
        handler: Handler(
          handlerFunc: (context, parameters) {
            final id = int.parse(parameters['id'][0]);
            return ProjectDetail(
              id: id,
            );
          },
        ),
      );
  }

  /// Get the right transition for each route
  static Widget transitionBuilder(
      BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {
    return FadeTransition(
      opacity: Tween(
        begin: 0.0,
        end: 1.0,
      ).animate(
        CurvedAnimation(
          parent: animation,
          curve: Interval(
            0.5,
            1,
            curve: Curves.easeOut,
          ),
        ),
      ),
      child: FadeTransition(
        opacity: Tween(
          begin: 1.0,
          end: 0.0,
        ).animate(
          CurvedAnimation(
            parent: secondaryAnimation,
            curve: Interval(
              0,
              0.5,
              curve: Curves.easeIn,
            ),
          ),
        ),
        child: child,
      ),
    );
  }
}
